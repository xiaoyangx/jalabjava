package urma.default_methods;

import javax.swing.*;

/**
 * Created by Adam on 7/15/2015.
 */

//look at the Collections.java interface to see the new default methods in structure view.
public class DefaultDriver {

    public static void main(String[] args) {

        Dude dude = new Dude();
        dude.punch();
        Boxable.shout();


    }

    //now we have multiple inheretience in Java, but if there is a conflict, the compiler will complain and you must override
    static class Dude implements Fightable, Boxable {

        @Override
        public void punch() {
            //System.out.println("Dude punch");
            Fightable.super.punch();
        }


    }
}


/*

A Virtual Extension Methods aka default aka defender methods are not the same as abstract methods.
//difference between Java8 interfaces with default methods and abstract classes
//http://viralpatel.net/blogs/java-8-default-methods-tutorial/
Abstract class can hold state of object.
It can have constructors and member variables. Whereas interfaces with Java 8 default methods cannot hold state

 */